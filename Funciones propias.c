#include <stdio.h>
#include <math.h>

double hermite(int n, double x) {
    if (n == 0) return 1.0;
    if (n == 1) return 2 * x;
    return 2 * x * hermite(n - 1, x) - 2 * (n - 1) * hermite(n - 2, x);
}

int main() {
    FILE *file = fopen("funciones_propias.csv", "w");
    if (file == NULL) {
        printf("Error al abrir el archivo.");
        return 1;
    }

    int n;
    double x, y;

    for (n = 0; n <= 3; ++n) {
        fprintf(file, "n=%d\n", n);
        for (x = -5.0; x <= 5.0; x += 0.1) {
            y = hermite(n, x) * exp(-x * x / 2.0) / sqrt(pow(2, n) * sqrt(M_PI) * tgamma(n + 1));
            fprintf(file, "%f,%f\n", x, y);
        }
        fprintf(file, "\n");
    }

    fclose(file);
    printf("Archivo CSV generado correctamente.");
    return 0;
}
