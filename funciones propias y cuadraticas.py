import csv
import matplotlib.pyplot as plt

# Función para leer y graficar un archivo CSV dado
def plot_csv(filename, title):
    with open(filename, 'r') as file:
        csv_reader = csv.reader(file)
        n = None
        x_values = []
        y_values = []
        for row in csv_reader:
            if row:
                if row[0].startswith('n='):
                    if n is not None:
                        plt.plot(x_values, y_values)
                        plt.xlabel('x')
                        plt.ylabel('y')
                        plt.title(f'{title} para n={n}')
                        plt.grid(True)
                        plt.show()
                    n = int(row[0][2:])
                    x_values = []
                    y_values = []
                else:
                    x_values.append(float(row[0]))
                    y_values.append(float(row[1]))

        # Graficar la última función del archivo
        plt.plot(x_values, y_values)
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title(f'{title} para n={n}')
        plt.grid(True)
        plt.show()

# Graficar funciones propias
plot_csv('funciones_propias.csv', 'Funciones Propias del Oscilador Armónico Cuántico')

# Graficar funciones propias cuadráticas
plot_csv('funciones_propias_cuadraticas.csv', 'Funciones Propias Cuadráticas del Oscilador Armónico Cuántico')
